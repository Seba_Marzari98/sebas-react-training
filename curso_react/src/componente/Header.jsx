import React from "react";
import { Menu } from 'antd';
import { HomeOutlined, UserOutlined, SettingOutlined } from '@ant-design/icons';


const Header = ({ setPage }) => {
    return (
        <div>
            <Menu mode="horizontal">
                <Menu.Item onClick={() => setPage('inicio') } key="inicio" icon={<HomeOutlined />}>
                    Inicio
                </Menu.Item>

                <Menu.Item onClick={() => setPage('sobre mi') } key="sobre mi" icon={<UserOutlined />}>
                    Sobre mi
                </Menu.Item>

                <Menu.Item onClick={() => setPage('mi cuenta') } key="mi cuenta" icon={<SettingOutlined />}>
                    Mi cuenta
                </Menu.Item>
            </Menu>
        </div>
    );
};

export default Header;