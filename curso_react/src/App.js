import React, {useState} from "react";
import Header from './componente/Header'
import AboutMe from './componente/AboutMe'
import MyAccount from './componente/MyAccount'
import './App.css';
import 'antd/dist/antd.css';
import './index.css';


function App() {

  const [pag, setPage]  = useState('inicio')
  return (
    <div className="App">
      <div className= 'App'>
            <Header setPage={setPage}/>
        </div>
        <div className= 'App'>
            <AboutMe setPage={setPage}/>
        </div>
        <div className= 'App'>
            <MyAccount setPage={setPage}/>
        </div>    
    </div>
  );
}

export default App;
