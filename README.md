# Getting Started with React. 



Fork the repository by clicking the + sign in the left of the screen

![Scheme](public/forkInstruction1.png)

Click Fork this repository located at the bottom of the side bar.

![Scheme](public/forkInstruction2.png)

Create a Proyect Name (ReactCourse)
In the name field put your name.

![Scheme](public/CreateFork.png)

And click Fork repository. This creates your own repository inside your proyect where you can clone and use this repository for practice.

## Running the aplication.

  Clone your repository and run the command `yarn` in your proyect root directory
  to install all the packages you need to run the aplication.

  After this run `yarn start` to start the aplication.
  It should look like this:

  ![Scheme](public/reactcompilation.png)

  And in the browser like this:

  ![Scheme](public/congratulation.png)


## Congratulations you're ready to learn React!


