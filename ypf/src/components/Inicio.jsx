import { Layout } from 'antd';
import React from 'react';

const { Footer, Content } = Layout;
var style = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "48px",
    width: "100%",
}

var phantom = {
    display: 'block',
    padding: '10%',
    height: '60px',
    width: '100%',
}


const HeaderYPF = () => {

    return (
        <Layout style={{ width: '100%' }}>
            <Layout className="site-layout">
                
                <Content style={{ margin: '0 16px' }} >
                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                        Bienvenido a los pedidos de Futit
                        </div>
                </Content>
                <div style={phantom} />
                <Footer style={style}>Ant Design ©2021  Created by Ant UED</Footer>
            </Layout>
        </Layout>
    );
};


export default HeaderYPF;