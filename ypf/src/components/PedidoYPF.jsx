import { Table } from 'antd';
import React from 'react';


const PedidoYPF = ({ productList, setSelectionType, selectionType }) => {


  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectionType(selectedRowKeys)
    }
  };

  const columns = [
    {
      title: 'Producto',
      dataIndex: 'producto',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Cantidad',
      dataIndex: 'cantidad',
      render: (text) => <a>{text}</a>,
    }
  ];

  return (
    <div>
      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        className="virtual-table"
        pagination={false}
        columns={columns}
        dataSource={productList}

      />
    </div>
  )
};

export default PedidoYPF;