import { Layout, Input, InputNumber, Button, Form, Typography } from 'antd';
import React, { useState } from 'react';
import PedidoYPF from './PedidoYPF'


const { Text, Link } = Typography;

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const { Header, Footer, Content } = Layout;
var style = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "48px",
    width: "100%",
}

var phantom = {
    display: 'block',
    padding: '10%',
    height: '60px',
    width: '100%',
}


const HeaderYPF = () => {
    const [selectionType, setSelectionType] = useState([]);
    const [productList, setProductList] = useState([]);
    const [key, setKey] = useState(1)

    const onFinish = (values) => {
        const obj = {
            key: key,
            producto: values.producto,
            cantidad: values.cantidad,
        }
        setKey(key + 1)
        setProductList([...productList, obj])
    };

    const onFinishFailed = (errorInfo) => {
    };

    const onDelete = () => {
        const array = productList.filter(item => {
            return (!selectionType.includes(item.key))
        })
        setProductList(array);
    }

    return (
        <Layout>
            <Layout className="site-layout">
                <Content style={{ margin: '0 16px' }} >
                    <div>
                        <Form {...layout}
                            style={{ display: 'flex' }}
                            name="basic"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}>
                            <Form.Item name="producto" style={{ width: "50%" }}>
                                <Input />
                            </Form.Item>

                            <Form.Item name="cantidad" style={{ width: "50%" }}>
                                <InputNumber min={0} max={50} defaultValue={0} />
                            </Form.Item>

                            <Button type="primary" htmlType="submit"> Agregar pedido </Button>

                            <Button type="primary" danger onClick={onDelete}> Eliminar pedido </Button>
                        </Form>
                        <PedidoYPF productList={productList} setSelectionType={setSelectionType} selectionType={selectionType} />
                    </div>
                </Content>
                <div style={phantom} />
                <Footer style={style}>Ant Design ©2021  Created by Ant UED</Footer>
            </Layout>
        </Layout>
    );
};


export default HeaderYPF;