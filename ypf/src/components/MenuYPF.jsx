import { Layout, Menu } from 'antd';
import {
    ShopOutlined,
    CarOutlined,
    CoffeeOutlined,
} from '@ant-design/icons';
import React, { useState } from 'react';


const { SubMenu } = Menu;
const { Sider } = Layout;


const MenuYpf = ({ setPage }) => {

    const [collapsed, setCollapsed] = useState(false);

    const onCollapse = collapsed => setCollapsed(collapsed);

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} style={{ height: '100%' }}>
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                <Menu.Item onClick={() => setPage('Inicio')} key="Inicio">Inicio</Menu.Item>
                <SubMenu key="YPF" icon={<CarOutlined />} title="YPF">
                    <Menu.Item onClick={() => setPage('HeaderYPF')} key="HeaderYPF">Hamburguesa YPF</Menu.Item>
                    <Menu.Item onClick={() => setPage('PedidoYPF')} key="PedidoYPF">Pedido YPF</Menu.Item>
                </SubMenu>
                <SubMenu key="Shell" icon={<CoffeeOutlined />} title="Shell">
                    <Menu.Item onClick={() => setPage('Pedir')} key="Pedir">Pedir Shell</Menu.Item>
                    <Menu.Item onClick={() => setPage('PedidoShell')} key="PedidoShell">Pedido Shell</Menu.Item>
                </SubMenu>
                <SubMenu key="Menu" icon={<ShopOutlined />} title="Menu">
                    <Menu.Item onClick={() => setPage('Fuegos')} key="Fuegos">Fuegos</Menu.Item>
                    <Menu.Item onClick={() => setPage('Fuegos')} key="Como en Casa">Como en Casa</Menu.Item>
                </SubMenu>
            </Menu>
        </Sider>
    );
}

export default MenuYpf