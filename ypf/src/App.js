import './App.css';
import HeaderYPF from './components/HeaderYPF'
import Inicio from './components/Inicio'
import MenuYpf from './components/MenuYPF'
import React, { useState } from 'react'
import { Layout, Menu } from 'antd';
import { Typography, Space } from 'antd';
const { Header, Footer, Content } = Layout;
const { Text, Link } = Typography;

function App() {

  const [pag, setPage] = useState('Inicio');

  return (
    <div className="App">
      <MenuYpf setPage={setPage} />
      <div className='column'>
        <Header className="site-layout-background" style={{ padding: 0 }}>
          <Text type="danger">Hacer Pedido</Text>
        </Header>
        {pag === 'Inicio' && <Inicio />}
        {pag === 'HeaderYPF' && <HeaderYPF />}
      </div>
    </div>
  );
}

export default App;
