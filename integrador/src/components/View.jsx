import { Table } from 'antd';
import React from 'react';

var style = {
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "48px",
    width: "100%",
    marginBottom: 16,
}




function View({ Word }) {

    const columns = [
        {
            title: 'Palabra',
            width: "10%",
            fixed: 'left',
            dataIndex: 'palabra',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Definicion',
            width: "45%",
            fixed: 'right',
            dataIndex: 'description',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Ejemplo',
            width: "45%",
            fixed: 'right',
            dataIndex: 'example',
            render: (text) => <a>{text}</a>,
        },

    ];

    return (
        <div style={{ style }} >
            <Table
                locale={{ emptyText: "No hay datos" }}
                className="virtual-table"
                pagination={false}
                columns={columns}
                dataSource={Word}

            />
        </div >
    )
};

export default View;