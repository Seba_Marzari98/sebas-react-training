import { Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { Button, Dropdown, Menu } from 'antd';
import { DeleteOutlined, DownOutlined, LeftOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

var style = {
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "48px",
    width: "100%",
    marginBottom: 16,
}

function List() {

    const [fav, setFav] = useState([]);
    const [selectionType, setSelectionType] = useState([]);

    useEffect(() => {
        let data = localStorage.getItem("Word");
        if (data != null) {
            const favor = JSON.parse(localStorage.getItem("Word"));
            setFav(favor);
        }
    }, [])

    const onDelete = () => {
        const array = fav.filter(item => {
            return (!selectionType.includes(item.key));
        })
        localStorage.setItem("Word", JSON.stringify(array));
        setFav(array);
    };

    const onDeleteAll = () => {
        const array = fav.filter(item => {
            return (!selectionType);
        })            
        localStorage.setItem("Word", JSON.stringify(array));
        setFav(array);
    };

    const menu = (
        <Menu>
            <Menu.Item key="1" icon={<DeleteOutlined />} onClick={onDelete} >
                Eliminar de Favoritos
            </Menu.Item>
            <br></br>
            <Menu.Item key="2" icon={<DeleteOutlined />} onClick={onDeleteAll} >
                Eliminar Todos de Favoritos
            </Menu.Item>
        </Menu>
    );

    const columns = [
        {
            title: 'Palabra',
            width: "10%",
            fixed: 'left',
            dataIndex: 'palabra',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Definicion',
            dataIndex: 'description',
            width: "80%",
            fixed: 'left',
            render: (text) => <a>{text}</a>,
        },
        {
            title: <Dropdown overlay={menu}>
                <Button icon={<DeleteOutlined />}>
                    <DownOutlined />
                </Button>
            </Dropdown>,
            width: "5%",
            fixed: 'right',
        },

        {
            title: <div><Button type="primary">
                <Link to='/' ><LeftOutlined /> Volver al Inicio  </Link>
            </Button></div>,
            width: "5%",
            fixed: 'right',
        },

    ];

    const rowSelection = {
        onChange: (selectedRowKeys, selectionType) => {
            setSelectionType(selectedRowKeys)
        }
    };

    function dynamicSort(property) {
        var sortOrder = 1;

        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }

        return function (a, b) {
            if (sortOrder === -1) {
                return b[property].localeCompare(a[property]);
            } else {
                return a[property].localeCompare(b[property]);
            }
        }
    }

    return (
        <div style={{ style }} >
            <Table locale={{emptyText:"No hay datos"}}
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                className="virtual-table"
                pagination={false}
                columns={columns}
                dataSource={fav.sort(dynamicSort("palabra"))}

            />
        </div >
    )
};

export default List;