import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Alert } from 'antd';
import { SearchOutlined, StarFilled } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import View from './View';


const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

function Word() {
    const [palabra, setPalabra] = useState([]);
    const [input, setInput] = useState('');
    const [aux, setAux] = useState('');
    const [fav, setFav] = useState([]);
    const [key, setKey] = useState(1)
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);
    const [word, setWord] = useState('');
    const [err, setErr] = useState(false);
    const [duplicated, setDuplicated] = useState(false);

    useEffect(() => {
        let data = localStorage.getItem("Word");
        if (data != null) {
            const favor = JSON.parse(localStorage.getItem("Word"));
            const id = favor.reduce((acc, current) => {
                return current.key > acc ? current.key : acc
            }, 0) + 1;
            setKey(id)
            setFav(favor);
        }
    }, [])

    useEffect(() => {
        setSuccess(false);
        setDuplicated(false)
        fetch(`https://api.dictionaryapi.dev/api/v2/entries/es/${input}`)
            .then(response => response.json())
            .then((data) => {
                if (data[0] && data[0].word) {
                    setError(false)
                    setPalabra(data[0]);
                    const obj = {
                        palabra: data[0].word,
                        description: data[0].meanings[0].definitions[0].definition,
                        example: data[0].meanings[0].definitions[0].example,
                    }
                    console.log("Data", data)
                    setWord([...word, obj]);
                } else {
                    setError(true)
                }
            });
    }, [input]);

    const onFinish = () => {
        setError(false)
        setDuplicated(false)
        setSuccess(false)
        if (palabra.length !== 0) {
            setErr(false)
            const obj = {
                key: key,
                palabra: palabra.word,
                description: palabra.meanings[0].definitions[0].definition,
            }
            if (!fav.find(i => i.palabra === obj.palabra)) {
                setSuccess(true);
                setKey(key + 1)
                setFav([...fav, obj]);
                localStorage.setItem("Word", JSON.stringify([...fav, obj]));
            }else{
                setDuplicated(true)
            }

        } else {
            setErr(true)
        }
    };

    const handleChange = (e) => {
        setInput(aux);
    };

    return (
        <div className="App">
            {error &&
                <Alert
                    message="No existe la palabra"
                    type="error"
                    closable
                />
            }
            {duplicated &&
                <Alert
                    message="La palabra ya esta en Favoritos"
                    type="error"
                    closable
                />
            }
            {err &&
                <Alert
                    message="No hay palabras que agregar"
                    type="error"
                    closable
                />
            }
            {success &&
                <Alert
                    message="Se agrego a Favoritos"
                    type="success"
                    closable
                />
            }
            <br></br>
            <div>
                <Form {...layout}
                    style={{ display: 'flex', justifyContent: 'center' }}
                    name="basic"
                    initialValues={{
                        remember: true,
                    }}
                >
                    <Form.Item name="producto">
                        <Input type="text" placeholder="Buscar Palabra" value={aux} onChange={(e) => setAux(e.target.value)} style={{ width: 200 }} />
                    </Form.Item>

                    <Button type="primary" icon={<SearchOutlined />} style={{ marginLeft: "4%", marginRight: "4%" }} onClick={handleChange} >
                        Buscar Palabra
                    </Button>
                    <Button type="primary" htmlType="submit" disabled={error} onClick={onFinish} style={{ marginRight: "4%" }}> Agregar a Favoritos </Button>

                    <Button type="primary">
                        <Link to='/Favoritos' className='sebalink'> Ir a Favoritos <StarFilled /> </Link>
                    </Button>
                </Form>
                <View Word={word} />
            </div>
        </div >
    );
}

export default Word;