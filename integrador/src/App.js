import Word from './components/Word';
import List from './components/List';
import './App.css';
import 'antd/dist/antd.css';
import './index.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';

const { Header, Footer, Content } = Layout;
var style = {
  backgroundColor: "#F8F8F8",
  borderTop: "1px solid #E7E7E7",
  textAlign: "center",
  padding: "20px",
  position: "fixed",
  left: "0",
  bottom: "0",
  height: "48px",
  width: "100%",
}

var styleHeader ={
  backgroundColor: "#F8F8F8",
  textAlign: "center",

}

function App() {
  return (
    <div>
      <Layout >
        <Header className={''} style={styleHeader}>
          Integrador Futit
        </Header>
        <Content>
          <BrowserRouter>
            <Switch>
              <Route path='/' exact component={Word} />
              <Route path='/Favoritos' component={List} />
              <Route path='/fireBase' />
            </Switch>
          </BrowserRouter>
        </Content>
        <Footer style={style}>
          Sebas Copyright  ©2021  Created by Ant UED
        </Footer>
      </Layout>
    </div>
  );
}

export default App;
