import './App.css';
import Header from './components/Header'
import AboutMe from './components/AboutMe'
import FireBase from './components/FireBase'
import { BrowserRouter, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>  
          <Route path='/' exact component={Header} />
          <Route path='/AboutMe' component={AboutMe} />
          <Route path='/fireBase' component={FireBase} />
        </Switch>
      </BrowserRouter>  
    </div>
  );
}

export default App;
