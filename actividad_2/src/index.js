import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyAKGjvKVMNceRLc1wfYHt6wD57SbdQhBRI",
  authDomain: "cursoreact-cdb5a.firebaseapp.com",
  projectId: "cursoreact-cdb5a",
  storageBucket: "cursoreact-cdb5a.appspot.com",
  messagingSenderId: "127159913062",
  appId: "1:127159913062:web:008ae653d1f0aa1d81bba7"
};

firebase.initializeApp(firebaseConfig)

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
