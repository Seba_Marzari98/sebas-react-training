import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';


const Header = () => {

    const [items, setItems] = useState([]);
    const [count, setCount] = useState(1);
    const [input1, setInput1] = useState('')

    const handleChange = (e) => {
        setCount(input1);
      };

    useEffect(() => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${count}`)
            .then(response => response.json())
            .then((data) => { setItems(data) });
    }, [count])
    

    return (
        <div className="App">
            <header className="App-header">
                <ul>
                    <li>
                        {items.name}
                    </li>
                </ul>   
                <br></br>
                <input type="text" value={input1} onChange={(e) => setInput1(e.target.value)}  />
                <button type="submit" onClick={handleChange}>
                    Ingresar
                </button>
                <br></br>
                <button type="primary" onClick={() => setCount(Number(count) + 1)}>
                Siguiente
                </button>
                <br></br>
                <button type="button" onClick={() => setCount(Number(count) - 1)}>
                    Anterior
                </button>
                <h1>{count}</h1>
                <div className='link-container'>
                    <Link to='/AboutMe' className='sebalink'> About Me </Link>
                </div>
            </header>


        </div >
    );
}

export default Header;