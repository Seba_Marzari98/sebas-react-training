import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import firebase from 'firebase';


const FireBase = () => {

    const [loadedfoods, setloadedfoods] = useState([]);

    useEffect(() => {
        const referenciaFoods = firebase.firestore().collection('foods')
        referenciaFoods.get().then((foods) => {
            const foodsData = foods.docs.map((food) => {
                return food.data();
            })
            setloadedfoods(foodsData);
        })
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                <ul>
                    {loadedfoods.map((foods) => {
                        return (<li>
                            {foods.name}
                        </li>)
                    })}
                </ul>
            </header>


        </div >
    );
}

export default FireBase;